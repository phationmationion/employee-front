import { Fragment, createElement } from 'react';
import { addDecorator, configure } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Fonts from '../src/Fonts';
import Styles from '../src/Styles';

// Add info decorator.
addDecorator(withInfo({
  inline: true,
  source: true,
  styles: {
    header: {
      body: {
        marginBottom: 0,
      },
    },
    infoStory: {
      backgroundColor: 'white',
      padding: 40,
    },
  },
}));

// Add the global styles.
addDecorator(story => createElement(
  Fragment,
  null,
  createElement(Fonts),
  createElement(Styles),
  story(),
));

/**
 * Loads the stories.
 */
function loadStories() {
  require('../src/index.story.ts');
}

/**
 * Initializes react-dates lib.
 */
function requireAntStyles() {
  require('antd/dist/antd.css');
}

function initStories() {
  requireAntStyles();
  loadStories();
}

// Apply configuration.
configure(initStories, module);
