const TsDocgenPlugin = require('react-docgen-typescript-webpack-plugin');
const { TsConfigPathsPlugin } = require('awesome-typescript-loader');
const path = require('path');

const rootDir = path.resolve(__dirname, '..');
const srcDir = path.resolve(rootDir, 'src');

module.exports = (_baseConfig, _env, config) => {
  config.module.rules.push(
    {
      test: /\.(ts|tsx)$/,
      include: srcDir,
      loader: require.resolve('awesome-typescript-loader'),
      resolve: {
        plugins: [
          new TsConfigPathsPlugin({
            configFileName: path.resolve(rootDir, 'tsconfig.json'),
          }),
        ],
      },
    },
    {
      test: /\.module\.sass$/,
      use: [
        { loader: 'style-loader' },
        {
          loader: 'css-loader',
          options: {
            importLoaders: 1,
            modules: true,
            localIdentName: '[path]-[local]__[hash:base64:5]',
          },
        },
        { loader: 'sass-loader', options: { modules: true } }
      ],
    }
  );

  config.plugins.push(new TsDocgenPlugin());

  config.resolve.extensions.push('.ts', '.tsx');

  return config;
};