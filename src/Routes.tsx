import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import AuthRoute from '@/containers/auth/AuthRoute';
import Employees from '@/views/Employees';
import LogIn from '@/views/LogIn';

/**
 * Component which contains routes of the application.
 */
const Routes = () => (
  <Switch>
    <Route path="/log-in" component={LogIn} />

    <AuthRoute path="/" component={Employees} />

    <Redirect to="/log-in" />
  </Switch>
);

// Export.
export default Routes;
