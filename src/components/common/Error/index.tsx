import React, { FC } from 'react';
import s from './styles.module.sass';

type Props = {};

const Error: FC<Props> = p => (
  <div className={s.root}>
    {p.children}
  </div>
);

export default Error;
