import React, { FC, ChangeEventHandler } from 'react';
import { Input as AntInput } from 'antd';
import { InputProps } from 'antd/lib/input';

interface Props extends Omit<InputProps, 'onChange'> {
  onChange: (value: string) => void;
}

const Input: FC<Props> = (p) => {
  const onChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    p.onChange(e.target.value);
  };
  return <AntInput {...p} onChange={onChange} />;
};

export default Input;
