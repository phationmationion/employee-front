import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import s from './styles.module.sass';

type Props = {};

const Logo: FC<Props> = () => (
  <Link className={s.root} to="/">
    <img className={s.img} />
  </Link>
);

export default Logo;
