import { Component, ReactNode } from 'react';
import orientationHelper from '@/helpers/orientation';

type Props = {
  /**
   * Allowed orientation
   */
  orientation: Omit<OrientationLockType, 'any' | 'natural'>;
  /**
   * Min width when orientation will not checked
   */
  activationWidth?: number;
  /**
   * Message wich will be shown instead of children when orientation is locked
   */
  lockMessage: ReactNode;
};

type State = {
  isLocked: boolean,
};

const defaultActivationWidth = 600;

export default class OrientationLock extends Component<Props> {

  state: State = {
    isLocked: false,
  };

  componentDidMount() {
    this.setIsLocked();
    window.addEventListener('resize', this.setIsLocked);
    window.addEventListener('orientationchange', this.setIsLocked);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setIsLocked);
    window.removeEventListener('orientationchange', this.setIsLocked);
  }

  setIsLocked = () => {
    const { orientation } = this.props;
    if (this.shouldCheckOrientation()) {
      this.setState({
        isLocked: !orientation.includes(orientationHelper.getType()),
      });
    } else if (this.state.isLocked) {
      this.setState({
        isLocked: false,
      });
    }
  }

  shouldCheckOrientation = () => {
    const { activationWidth = defaultActivationWidth } = this.props;
    const { width } = (document.documentElement as HTMLHtmlElement).getBoundingClientRect();
    return width <= activationWidth;
  }

  render() {
    return (
      this.state.isLocked
        ? this.props.lockMessage
        : this.props.children
    );
  }
}
