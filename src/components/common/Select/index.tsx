import React, { FC } from 'react';
import { Select as AntSelect } from 'antd';
import { SelectProps } from 'antd/lib/select';
import classnames from 'classnames';

const s = require('./styles.module.sass');

/**
 * Props of the Select
 */
interface Props extends SelectProps {
  /**
   * Size of the margin-right
   */
  margin?: 'm';
}

/**
 * Component of the Select
 */
const Select: FC<Props> = p => (
  <AntSelect
    {...p}
    className={classnames(s.root, {
      [p.className as string]: p.className,
      [s[`margin-${p.margin}`]]: p.margin,
    })}
  />
);

const { Option } = AntSelect;

export default Select;
export { Option };
