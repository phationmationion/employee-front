import React, { FC } from 'react';

type Props = {
  height: number;
};

const Spacer: FC<Props> = p => (
  <div
    style={{
      width: '100%',
      height: `${p.height}px`,
    }}
  />
);

export default Spacer;
