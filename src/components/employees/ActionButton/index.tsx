import React, { FC } from 'react';
import s from './styles.module.sass';
import Button from '@/components/common/Button';
import { PostStatusData } from '@/services/statuses';

type Props = {
  idEmployee: string;
  idStatus: string;
  onAction: (data: PostStatusData) => void;
  children: string;
};

const ActionButton: FC<Props> = (p: Props) => {
  const onClick = () => p.onAction({
    id_employee: p.idEmployee,
    id_status: p.idStatus,
  });

  return (
    <Button className={s.root} onClick={onClick}>
      {p.children}
    </Button>
  );
};

export default ActionButton;
