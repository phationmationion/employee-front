import React from 'react';
import { storiesOf } from '@storybook/react';
import { Select } from 'antd';
import Filter from './index';

const { Option } = Select;

/**
 * Handler for Select component
 * @param value
 */
const handlerSelect = (selectedValue: string) => {};

export default (path: string) => storiesOf(`${path}/Filter`, module)
  .add('example', () => (
    <Filter title="Объект">
      <Select
        value="match"
        onChange={handlerSelect}
      >
        <Option value="match">
          Соответствует
        </Option>
        <Option value="no-match">
          Не соответствует
        </Option>
      </Select>
      <Select
        value={'second'}
        onChange={handlerSelect}
      >
        <Option value="first">
          Объект 1
        </Option>
        <Option value="second">
          Объект 2
        </Option>
      </Select>
    </Filter>
  ));
