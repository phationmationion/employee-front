import React, { FC } from 'react';
import classnames from 'classnames';

import s from './styles.module.sass';

/**
 * Props of the Filter
 */
type Props = {
  /**
   * Title of the filter
   */
  title: string;

  /**
   * Flag for margin-right
   */
  margin?: boolean;
};

/**
 * Component of the Filter
 */
const Filter: FC<Props> = p => (
  <div className={classnames(s.root, { [s.margin]: p.margin })}>
    <div className={s.title}>
      {p.title}
    </div>
    <div className={s.controls}>
      {p.children}
    </div>
  </div>
);

export default Filter;
