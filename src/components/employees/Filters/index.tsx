import React, { FC, ReactNode } from 'react';
import s from './styles.module.sass';

type Props = {
  object: ReactNode;
  status: ReactNode;
  date: ReactNode;
  pageSize: ReactNode;
};

/**
 * Component of the Filters
 */
const Filters: FC<Props> = p => (
  <>
    <div className={s.row}>
      <div className={s.item}>
        {p.object}
      </div>
      <div className={s.item}>
        {p.status}
      </div>
    </div>
    <div className={s.row}>
      <div className={s.item}>
        {p.date}
      </div>
      <div className={s.item}>
        {p.pageSize}
      </div>
    </div>
  </>
);

export default Filters;
