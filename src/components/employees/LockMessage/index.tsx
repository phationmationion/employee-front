import React, { FC } from 'react';
import s from './styles.module.sass';

type Props = {};

const LockMessage: FC<Props> = p => (
  <div className={s.root}>
    {p.children}
  </div>
);

export default LockMessage;
