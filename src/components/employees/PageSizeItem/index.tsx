import React, { FC, MouseEventHandler } from 'react';
import classnames from 'classnames';

const s = require('./styles.module.sass');

/**
 * Props of the PageSizeItem
 */
type Props = {
  /**
   * True if item is active
   */
  isActive: boolean;

  /**
   * Item click handler
   */
  onClick: MouseEventHandler<HTMLDivElement>;
};

/**
 * Component of the PageSizeItem
 */
const PageSizeItem: FC<Props> = ({ isActive, children, onClick }) => (
  <div
    className={classnames(s.root, { [s.isActive]: isActive })}
    onClick={onClick}
  >
    {children}
  </div>
);

export default PageSizeItem;
