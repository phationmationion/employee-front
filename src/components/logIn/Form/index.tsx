import React, { FC, ReactNode, FormEventHandler } from 'react';
import Card from '@/components/common/Card';
import Button from '@/components/common/Button';
import Error from '@/components/common/Error';

import s from './styles.module.sass';

type Props = {
  title: string;
  email: ReactNode;
  password: ReactNode;
  emailText: string;
  passwordText: string;
  error?: string;
  buttonText: string;
  onSubmit: FormEventHandler<HTMLFormElement>;
};

const Form: FC<Props> = p => (
  <Card className={s.root} title={p.title}>
    <form onSubmit={p.onSubmit}>
      <div className={s.item}>
        {p.email}
      </div>
      <div className={s.item}>
        {p.password}
      </div>
      {p.error && (
        <div className={s.item}>
          <Error>
            {p.error}
          </Error>
        </div>
      )}
      <div className={s.item}>
        <Button htmlType="submit">
          {p.buttonText}
        </Button>
      </div>
    </form>
  </Card>
);

export default Form;
