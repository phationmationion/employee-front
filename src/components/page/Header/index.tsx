import React, { FC } from 'react';
import classnames from 'classnames';
import s from './styles.module.sass';

import Logo from '@/components/common/Logo';
import Button from '@/components/common/Button';
import Typography from '@/components/common/Typography';

type Props = {
  isAuth: boolean;
  centeredLogo?: boolean;
  logoutText: string;
  userName?: string;
  onLogOut: () => void;
};

const Header: FC<Props> = p => (
  <header className={classnames(s.root, { [s.centered]: p.centeredLogo })}>
    <Logo />
    {p.isAuth && (
      <div className={s.right}>
        <Typography.Text className={s.userName}>
          {p.userName}
        </Typography.Text>
        <Button onClick={p.onLogOut}>
          {p.logoutText}
        </Button>
      </div>
    )}
  </header>
);

export default Header;
