import { selectors } from '@/entities/auth';
import { RootState } from '@/entities/root';
import React, { ComponentPropsWithoutRef, FC } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router-dom';

interface MapStateToProps {
  isAuth: RootState['auth']['isAuth'];
}

/**
 * Component props.
 */
interface AuthRouteProps extends MapStateToProps, RouteProps { }

/**
 * The AuthRoute component.
 */
const AuthRoute: FC<AuthRouteProps> = ({
  isAuth,
  ...rest
}) => isAuth ? <Route {...rest} /> : <Redirect to={'/log-in'} />;

const mapStateToProps = (state: RootState): MapStateToProps => ({
  isAuth: selectors.selectIsAuth(state),
});

// Exported props.
type ExportedProps = Omit<ComponentPropsWithoutRef<typeof AuthRoute>, keyof MapStateToProps>;

// Export.
export { ExportedProps as AuthRouteProps };
export default connect(mapStateToProps)(AuthRoute);
