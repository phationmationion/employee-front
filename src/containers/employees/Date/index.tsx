import React, { FC } from 'react';
import { connect } from 'react-redux';
import { Moment } from 'moment';

import { dateTitle, datePlaceholder } from '@/content/employees.json';

import { RootState } from '@/entities/root';

import { EmployeesFetchData } from '@/services/employees';
import { selectors, actions } from '@/entities/employees';

import RangePicker from '@/components/common/RangePicker';
import Filter from '@/components/employees/Filter';

/**
 * Props of the DateContainer
 */
type Props = {
  /**
   * Props of the RangePicker
   */
  date?: [Moment, Moment];

  /**
   * Data of last employees table fetching
   */
  data: EmployeesFetchData;

  /**
   * Action for fetch employees table content
   */
  fetchEmployees: (data: EmployeesFetchData) => void;
};

/**
 * Component of the DateContainer
 */
const DateContainer: FC<Props> = (p) => {
  const onChange = (date: [Moment, Moment]) => {
    p.fetchEmployees({ ...p.data, date });
  };

  return (
    <Filter title={dateTitle} margin={true}>
      <RangePicker
        placeholder={datePlaceholder}
        value={p.date}
        onChange={onChange}
      />
    </Filter>
  );
};

export default connect(
  (state: RootState) => ({
    data: selectors.selectEmployeesFetchData(state),
    date: selectors.selectDate(state),
  }),
  {
    fetchEmployees: actions.fetchEmployees,
  },
)(DateContainer);
