import React, { Component } from 'react';
import { connect } from 'react-redux';

import { RootState } from '@/entities/root';

import {
  objectTitle,
  objectPlaceholder,
} from '@/content/employees.json';

import { EmployeesFetchData } from '@/services/employees';
import { Object as ObjectType } from '@/services/objects';
import { selectors, actions } from '@/entities/employees';

import Select, { Option } from '@/components/common/Select';
import Filter from '@/components/employees/Filter';

/**
 * Props of the ObjectContainer
 */
type Props = {
  /**
   * Value of the object
   */
  object?: string;

  /**
   * Options for the Objects select
   */
  objects: ObjectType[];

  /**
   * Data of last employees table fetching
   */
  data: EmployeesFetchData;

  /**
   * Action for fetch employees table content
   */
  fetchEmployees: (data: EmployeesFetchData) => void;

  /**
   * Action for fetch objects
   */
  fetchObjects: () => void;
};

/**
 * Component of the ObjectContainer
 */
class ObjectContainer extends Component<Props> {

  componentDidMount() {
    this.props.fetchObjects();
  }

  handleChangeObject = (object: string) => {
    this.props.fetchEmployees({ ...this.props.data, object });
  }

  render() {
    const { object, objects } = this.props;

    return (
      <Filter title={objectTitle} margin={true}>
        <Select
          value={object}
          placeholder={objectPlaceholder}
          onChange={this.handleChangeObject}
          allowClear={true}
        >
          {objects.map((object: ObjectType) => (
            <Option value={object.id} key={object.id}>
              {object.name}
            </Option>
          ))}
        </Select>
      </Filter>
    );
  }
}

export default connect(
  (state: RootState) => ({
    data: selectors.selectEmployeesFetchData(state),
    object: selectors.selectObject(state),
    objects: selectors.selectObjects(state),
  }),
  {
    fetchEmployees: actions.fetchEmployees,
    fetchObjects: actions.fetchObjects,
  },
)(ObjectContainer);
