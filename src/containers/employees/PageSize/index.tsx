import React, { FC } from 'react';
import { connect } from 'react-redux';

import { pageSizeTitle } from '@/content/employees.json';
import { RootState } from '@/entities/root';

import { EmployeesFetchData } from '@/services/employees';
import { selectors, actions } from '@/entities/employees';

import Filter from '@/components/employees/Filter';
import PageSizeItem from '@/components/employees/PageSizeItem';

/**
 * Props of the PageSizeContainer
 */
type Props = {
  /**
   * Quantity of the rows in table
   */
  value: number;

  /**
   * Data of last employees table fetching
   */
  data: EmployeesFetchData;

  /**
   * Action for fetch employees table content
   */
  fetchEmployees: (data: EmployeesFetchData) => void;
};

/**
 * Array with available rowsQantity values
 */
const items = [20, 50, 100];

/**
 * Component of the PageSizeContainer
 */
const PageSizeContainer: FC<Props> = p => (
  <Filter title={pageSizeTitle}>
    <div>
      {items.map((item: number) => {
        const isActive = item === p.value;

        const onClick = () => {
          if (isActive) return;
          p.fetchEmployees({ ...p.data, pageSize: item });
        };

        return (
          <PageSizeItem
            isActive={isActive}
            onClick={onClick}
            key={item}
          >
            {item}
          </PageSizeItem>
        );
      })}
    </div>
  </Filter>
);

export default connect(
  (state: RootState) => ({
    data: selectors.selectEmployeesFetchData(state),
    value: selectors.selectPageSize(state),
  }),
  {
    fetchEmployees: actions.fetchEmployees,
  },
)(PageSizeContainer);
