import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  statusTitle,
  statusPlaceholder,
} from '@/content/employees.json';

import { RootState } from 'entities/root';

import { EmployeesFetchData } from '@/services/employees';
import { Status as StatusType } from '@/services/statuses';
import { selectors, actions } from '@/entities/employees';

import Select, { Option } from '@/components/common/Select';
import Filter from '@/components/employees/Filter';

/**
 * Props of the Status
 */
type Props = {
  /**
   * Value of the status
   */
  status: string;
  /**
   * Value of the status
   */
  statuses: StatusType[];

  /**
   * Data of last employees table fetching
   */
  data: EmployeesFetchData;

  /**
   * Action for fetch employees table content
   */
  fetchEmployees: (data: EmployeesFetchData) => void;

  /**
   * Action for fetch statuses
   */
  fetchStatuses: () => void;
};

/**
 * Component of the Status
 */
class Status extends Component<Props> {

  componentDidMount() {
    this.props.fetchStatuses();
  }

  handleChangeStatus = (status: string) => {
    this.props.fetchEmployees({ ...this.props.data, status });
  }

  render() {
    const { status, statuses } = this.props;

    return (
      <Filter title={statusTitle}>
        <Select
          value={status}
          placeholder={statusPlaceholder}
          onChange={this.handleChangeStatus}
          allowClear={true}
        >
          {statuses.map((status: StatusType) => (
            <Option value={status.id} key={status.id}>
              {status.name}
            </Option>
          ))}
        </Select>
      </Filter>
    );
  }
}

export default connect(
  (state: RootState) => ({
    data: selectors.selectEmployeesFetchData(state),
    statuses: selectors.selectStatuses(state),
    status: selectors.selectStatus(state),
  }),
  {
    fetchEmployees: actions.fetchEmployees,
    fetchStatuses: actions.fetchStatuses,
  },
)(Status);
