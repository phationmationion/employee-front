import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PaginationConfig, SorterResult } from 'antd/lib/table';

import { RootState } from '@/entities/root';

import { EmployeesFetchData, Employee, EmployeeAction } from '@/services/employees';
import { PostStatusData } from '@/services/statuses';
import { actions, selectors } from '@/entities/employees';

import Table from '@/components/common/Table';
import ActionButton from '@/components/employees/ActionButton';
import ActionsCell from '@/components/employees/ActionsCell';

/**
 * Props of the TableContainer
 */
type Props = {
  /**
   * Columns of the table
   */
  columns: any[];

  /**
   * Data for table body
   */
  dataSource: any[];

  /**
   * Data for last employees table fetching
   */
  data: EmployeesFetchData;

  /**
   * Table content loading flag
   */
  loading: boolean;

  /**
   * Action for fetch employees table content
   */
  fetchEmployees: (data: EmployeesFetchData) => void;

  /**
   * Action for post employee status
   */
  postStatus: (data: PostStatusData) => void;
};

/**
 * State of the TableContainer
 */
interface State {
  /**
   * Flag for small table sizing
   */
  small: boolean;
}

/**
 * Component of the TableContainer
 */
class TableContainer extends Component<Props> {

  state: State = {
    small: false,
  };

  componentDidMount() {
    this.props.fetchEmployees(this.props.data);
    this.setSize();
    window.addEventListener('resize', this.setSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setSize);
  }

  setSize = () => {
    const { width } = (document.documentElement as HTMLHtmlElement).getBoundingClientRect();

    this.setState({
      small: width <= 800,
    });
  }

  handleTableChange(pagination: PaginationConfig, filters: Record<any, string[]>, sorter: SorterResult<any>) {
    this.props.fetchEmployees({
      ...this.props.data,
      total: pagination.pageSize,
      current: pagination.current,
      sortField: sorter.field,
      sortOrder: sorter.order,
    });
  }

  render() {
    const { columns, dataSource, loading } = this.props;
    const buttonsColumn = {
      title: 'Действия',
      align: 'center',
      key: 'actions',
      width: this.state.small ? 130 : 400,
      render: (_: any, employee: Employee) => (
        <ActionsCell>
          {employee.actions.map((action: EmployeeAction) => (
            <ActionButton
              onAction={this.props.postStatus}
              idEmployee={employee.id}
              idStatus={action.id}
              key={action.id}
            >
              {action.name}
            </ActionButton>
          ))}
        </ActionsCell>
      ),
    };

    return (
      <Table
        columns={[...columns, buttonsColumn]}
        dataSource={dataSource}
        loading={loading}
        scroll={{ x: this.state.small ? 930 : 1200 }}
        pagination={{
          style: { float: 'left' },
        }}
        onChange={this.handleTableChange}
      />
    );
  }
}

export default connect(
  (state: RootState) => ({
    data: selectors.selectEmployeesFetchData(state),
    columns: selectors.selectColumns(state),
    statuses: selectors.selectStatuses(state),
    dataSource: selectors.selectDataSource(state),
    loading: selectors.selectLoading(state),
  }),
  {
    fetchEmployees: actions.fetchEmployees,
    postStatus: actions.postStatus,
  },
)(TableContainer);
