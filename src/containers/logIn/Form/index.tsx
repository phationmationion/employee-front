import React, { FC, FormEventHandler, FormEvent } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { title, buttonText, emailText, passwordText } from '@/content/logIn.json';
import { selectors, actions } from '@/entities/logIn';
import { selectors as authSelectors } from '@/entities/auth';
import { RootState } from '@/entities/root';

import Input from '@/components/common/Input';
import Form from '@/components/logIn/Form';
import { LogInData } from '@/services/logIn';

type Props = {
  email: string;
  password: string;
  error: string;
  isAuth: boolean;
  logIn: (data: LogInData) => void;
  changeEmail: (email: string) => void;
  changePassword: (password: string) => void;
};

const FormContainer: FC<Props> = (p) => {
  if (p.isAuth) return <Redirect to="/" />;

  const onSubmit: FormEventHandler = (e: FormEvent) => {
    e.preventDefault();
    p.logIn({ email: p.email, password: p.password });
  };

  return (
    <Form
      title={title}
      buttonText={buttonText}
      emailText={emailText}
      passwordText={passwordText}
      email={
        <Input
          value={p.email}
          placeholder={emailText}
          onChange={p.changeEmail}
        />
      }
      password={
        <Input
          value={p.password}
          placeholder={passwordText}
          onChange={p.changePassword}
          type="password"
        />
      }
      error={p.error}
      onSubmit={onSubmit}
    />
  );
};

export default connect(
  (state: RootState) => ({
    email: selectors.selectEmail(state),
    password: selectors.selectPassword(state),
    error: selectors.selectError(state),
    isAuth: authSelectors.selectIsAuth(state),
  }),
  {
    changeEmail: actions.changeEmail,
    changePassword: actions.changePassword,
    logIn: actions.logIn,
  },
)(FormContainer);
