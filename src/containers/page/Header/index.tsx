import React, { FC } from 'react';
import { connect } from 'react-redux';
import { logoutText } from '@/content/header.json';
import { RootState } from '@/entities/root';
import { actions, selectors } from '@/entities/auth';
import Header from '@/components/page/Header';

type Props = {
  isAuth: boolean;
  userName?: string;
  centeredLogo?: boolean;
  logOut: () => void;
};

const HeaderContainer: FC<Props> = p => (
  <Header
    logoutText={logoutText}
    centeredLogo={p.centeredLogo}
    userName={p.userName}
    onLogOut={p.logOut}
    isAuth={p.isAuth}
  />
);

export default connect(
  (state: RootState) => ({
    userName: 'user name', // selectors.selectUserName(state),
    isAuth: selectors.selectIsAuth(state),
  }),
  {
    logOut: actions.logOut,
  },
)(HeaderContainer);
