import React, { FC } from 'react';

import Header from '@/containers/page/Header';

import PageWrapper from '@/components/page/PageWrapper';
import ContentWrapper from '@/components/page/ContentWrapper';
import Footer from '@/components/page/Footer';

type Props = {
  centeredLogo?: boolean;
};

const PageWrapperContainer: FC<Props> = p => (
  <PageWrapper>
    <Header centeredLogo={p.centeredLogo} />
    <ContentWrapper>
      {p.children}
    </ContentWrapper>
    <Footer />
  </PageWrapper>
);

export default PageWrapperContainer;
