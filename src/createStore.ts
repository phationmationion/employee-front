import reducer from '@/entities/root';
import { applyMiddleware, compose, createStore as createReduxStore } from 'redux';
import thunk from 'redux-thunk';

const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose || compose;

/**
 * Creates a new Redux store of this application.
 */
function createStore() {
  const middlewares = applyMiddleware(
    thunk,
  );

  const store = createReduxStore(reducer, composeEnhancers(middlewares));
  return store;
}

// Export.
export default createStore;
