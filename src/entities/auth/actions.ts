import { createStandardAction } from 'typesafe-actions';
import { LogInResponse } from '@/services/logIn';

export const logIn = createStandardAction('auth_LOG_IN')<LogInResponse>();

export const logOut = createStandardAction('auth_LOG_OUT')<undefined>();
