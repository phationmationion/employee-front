import * as selectors from './selectors';
import { getType } from 'typesafe-actions';
import * as standartActions from './actions';

export interface State {
  isAuth: boolean;
}

const DEFAULT_STATE: State = {
  isAuth: false,
};

const reducer = (state: State = DEFAULT_STATE, action: any): State => {
  switch (action.type) {
    case getType(standartActions.logIn): {
      localStorage.setItem('token', action.payload.access.token);
      localStorage.setItem('tokenExp', action.payload.access.exp);
      localStorage.setItem('refreshToken', action.payload.refresh.token);
      localStorage.setItem('refreshTokenExp', action.payload.refresh.exp);

      return ({
        ...state,
        isAuth: true,
      });
    }

    case getType(standartActions.logOut): {
      localStorage.removeItem('token');
      localStorage.removeItem('tokenExp');
      localStorage.removeItem('refreshToken');
      localStorage.removeItem('refreshTokenExp');

      return ({
        ...state,
        isAuth: false,
      });
    }

    default: return state;
  }
};

// Export.
export {
  selectors,
  standartActions as actions,
};
export default reducer;
