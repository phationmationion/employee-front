import { RootState } from '@/entities/root';

export const selectIsAuth = (state: RootState) => (
  state.auth.isAuth
);
