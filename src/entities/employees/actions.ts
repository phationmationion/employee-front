import { createAsyncAction } from 'typesafe-actions';
import { EmployeesFetchData } from '@/services/employees';
import { StatusesResponse, PostStatusData } from '@/services/statuses';
import { ObjectsResponse } from '@/services/objects';

export const loadEmployees = createAsyncAction(
  'employees_LOAD_EMPLOYEES_REQUEST',
  'employees_LOAD_EMPLOYEES_SUCCESS',
  'employees_LOAD_EMPLOYEES_FAILURE',
)<EmployeesFetchData, any, any>();

export const loadStatuses = createAsyncAction(
  'employees_LOAD_STATUSES_REQUEST',
  'employees_LOAD_STATUSES_SUCCESS',
  'employees_LOAD_STATUSES_FAILURE',
)<undefined, StatusesResponse, any>();

export const loadObjects = createAsyncAction(
  'employees_LOAD_OBJECTS_REQUEST',
  'employees_LOAD_OBJECTS_SUCCESS',
  'employees_LOAD_OBJECTS_FAILURE',
)<undefined, ObjectsResponse, any>();

export const setStatus = createAsyncAction(
  'employees_SET_STATUS_REQUEST',
  'employees_SET_STATUS_SUCCESS',
  'employees_SET_STATUS_FAILURE',
)<undefined, PostStatusData, any>();
