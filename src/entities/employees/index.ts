import { ActionType, getType } from 'typesafe-actions';
import { Moment } from 'moment';
import { Status } from '@/services/statuses';
import { Object } from '@/services/objects';
import { Employee } from '@/services/employees';

import * as standartActions from './actions';
import * as thunks from './thunks';
import * as selectors from './selectors';

export interface State {
  columns: any;
  employees: string[];
  employeesById: {};

  object?: string;
  objectsById: {};
  objects: string[];

  status?: string;
  statusesById: {};
  statuses: string[];

  date?: [Moment, Moment];
  sortField?: string;
  sortOrder?: string;
  pagination: {
    pageSize: number;
    current?: number;
    total?: number;
  };

  loading: boolean;
  error: boolean;
}

type Action = ActionType<typeof standartActions>;

const DEFAULT_STATE: State = {
  columns: [],
  employeesById: {},
  employees: [],
  date: undefined,
  object: undefined,
  objectsById: {},
  objects: [],
  statusesById: {},
  statuses: [],
  status: undefined,
  sortField: undefined,
  sortOrder: undefined,
  pagination: {
    pageSize: 20,
    current: undefined,
    total: undefined,
  },
  loading: false,
  error: false,
};

const reducer = (state: State = DEFAULT_STATE, action: Action): State => {
  switch (action.type) {
    case getType(standartActions.loadEmployees.request): return ({
      ...state,
      date: action.payload.date,
      object: action.payload.object,
      status: action.payload.status,
      pagination: {
        pageSize: action.payload.pageSize,
        current: action.payload.current,
        total: action.payload.total,
      },
      sortField: action.payload.sortField,
      sortOrder: action.payload.sortOrder,
      loading: true,
      error: false,
    });

    case getType(standartActions.loadEmployees.success): return ({
      ...state,
      columns: action.payload.columns,
      employeesById: action.payload.list.reduce((byId: {}, employee: Employee) => {
        byId[employee.id] = employee;
        return byId;
      }, {}),
      employees: action.payload.list.map((employee: Employee) => employee.id),
      loading: false,
    });

    case getType(standartActions.loadEmployees.failure): return ({
      ...state,
      loading: false,
      error: true,
    });

    case getType(standartActions.loadStatuses.success): return ({
      ...state,
      statusesById: action.payload.list.reduce((byId: {}, status: Status) => {
        byId[status.id] = status;
        return byId;
      }, {}),
      statuses: action.payload.list.map((status: Status) => status.id),
    });

    case getType(standartActions.loadObjects.success): return ({
      ...state,
      objectsById: action.payload.list.reduce((byId: {}, object: Object) => {
        byId[object.id] = object;
        return byId;
      }, {}),
      objects: action.payload.list.map((object: Object) => object.id),
    });

    case getType(standartActions.setStatus.success): {
      const { id_employee, id_status } = action.payload;
      return {
        ...state,
        employeesById: {
          ...state.employeesById,
          [id_employee]: {
            ...state.employeesById[id_employee],
            status: id_status,
          },
        },
      };
    }

    default: return state;
  }
};

const exportActions = {
  ...standartActions,
  ...thunks,
};

// Export.
export {
  exportActions as actions,
  selectors,
  standartActions,
};
export default reducer;
