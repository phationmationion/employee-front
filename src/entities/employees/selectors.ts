import { createSelector } from 'reselect';
import { RootState } from '@/entities/root';

const selectEmployeesById = (state: RootState) => (
  state.employees.employeesById
);

const selectEmployeesIds = (state: RootState) => (
  state.employees.employees
);

export const selectColumns = (state: RootState) => (
  state.employees.columns
);

export const selectDate = (state: RootState) => (
  state.employees.date
);

export const selectObject = (state: RootState) => (
  state.employees.object
);

const selectObjectsById = (state: RootState) => (
  state.employees.objectsById
);

const selectObjectsIds = (state: RootState) => (
  state.employees.objects
);

const selectStatusesById = (state: RootState) => (
  state.employees.statusesById
);

const selectStatusesIds = (state: RootState) => (
  state.employees.statuses
);

export const selectStatus = (state: RootState) => (
  state.employees.status
);

export const selectSortField = (state: RootState) => (
  state.employees.sortField
);

export const selectSortOrder = (state: RootState) => (
  state.employees.sortOrder
);

export const selectPageSize = (state: RootState) => (
  state.employees.pagination.pageSize
);

export const selectPaginationCurrent = (state: RootState) => (
  state.employees.pagination.current
);

export const selectPaginationTotal = (state: RootState) => (
  state.employees.pagination.total
);

export const selectLoading = (state: RootState) => (
  state.employees.loading
);

export const selectObjects = createSelector(
  selectObjectsById,
  selectObjectsIds,
  (byId, ids) => ids.map(id => byId[id]),
);

export const selectStatuses = createSelector(
  selectStatusesById,
  selectStatusesIds,
  (byId, ids) => ids.map(id => byId[id]),
);

export const selectDataSource = createSelector(
  selectEmployeesById,
  selectEmployeesIds,
  selectStatusesById,
  (byId, ids, statuses) => ids.map((id: string) => {
    const status = statuses[byId[id].status];
    return {
      ...byId[id],
      status: status && status.name,
    };
  }),
);

export const selectEmployeesFetchData = createSelector(
  selectDate,
  selectObject,
  selectStatus,
  selectPageSize,
  selectPaginationCurrent,
  selectPaginationTotal,
  selectSortField,
  selectSortOrder,
  (date, object, status, pageSize, current, total, sortField, sortOrder) => ({
    date,
    object,
    status,
    pageSize,
    current,
    total,
    sortField,
    sortOrder,
  }),
);
