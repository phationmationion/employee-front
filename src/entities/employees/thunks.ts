import * as standardActions from './actions';
import { RootAction, RootState } from '@/entities/root';
import { ThunkAction } from 'redux-thunk';
import { getEmployees, EmployeesFetchData } from '@/services/employees';
import {
  getStatuses,
  StatusesResponse,
  postStatus as postStatusService,
  PostStatusData,
} from '@/services/statuses';
import { getObjects, ObjectsResponse } from '@/services/objects';

export const fetchEmployees = (data: EmployeesFetchData): ThunkAction<void, RootState, undefined, RootAction> => (
  (dispatch, getState) => {
    dispatch(standardActions.loadEmployees.request(data));

    return getEmployees(data).then(
      (response: any) => dispatch(standardActions.loadEmployees.success(response)),
      (error: any) => dispatch(standardActions.loadEmployees.failure(error)),
    );
  }
);

export const fetchStatuses = (): ThunkAction<void, RootState, undefined, RootAction> => (
  (dispatch, getState) => {
    dispatch(standardActions.loadStatuses.request());

    return getStatuses().then(
      (response: StatusesResponse) => dispatch(standardActions.loadStatuses.success(response)),
      (error: any) => dispatch(standardActions.loadStatuses.failure(error)),
    );
  }
);

export const fetchObjects = (): ThunkAction<void, RootState, undefined, RootAction> => (
  (dispatch, getState) => {
    dispatch(standardActions.loadObjects.request());

    return getObjects().then(
      (response: ObjectsResponse) => dispatch(standardActions.loadObjects.success(response)),
      (error: any) => dispatch(standardActions.loadObjects.failure(error)),
    );
  }
);

export const postStatus = (data: PostStatusData): ThunkAction<void, RootState, undefined, RootAction> => (
  (dispatch, getState) => {
    dispatch(standardActions.setStatus.request());

    return postStatusService(data).then(
      () => dispatch(standardActions.setStatus.success(data)),
      (error: any) => dispatch(standardActions.setStatus.failure(error)),
    );
  }
);
