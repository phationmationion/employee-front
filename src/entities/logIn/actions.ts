import { createStandardAction } from 'typesafe-actions';

export const changeEmail = createStandardAction('login_CHANGE_EMAIL')<string>();

export const changePassword = createStandardAction('login_CHANGE_PASSWORD')<string>();

export const changeError = createStandardAction('login_CHANGE_ERROR')<string>();

export const clearForm = createStandardAction('login_CLEAR_FORM')<undefined>();
