import { ActionType, getType } from 'typesafe-actions';
import { DeepReadonly } from 'utility-types';

import * as standartActions from './actions';
import * as selectors from './selectors';
import * as thunks from './thunks';

type State = DeepReadonly<{
  email: string;
  password: string;
  error: string;
}>;

type Action = ActionType<typeof standartActions>;

const DEFAULT_STATE: State = {
  email: '',
  password: '',
  error: '',
};

const reducer = (state: State = DEFAULT_STATE, action: Action): State => {
  switch (action.type) {
    case getType(standartActions.changeEmail):
      return {
        ...state,
        email: action.payload,
      };

    case getType(standartActions.changePassword):
      return {
        ...state,
        password: action.payload,
      };

    case getType(standartActions.changeError):
      return {
        ...state,
        error: action.payload,
      };

    case getType(standartActions.clearForm):
      return DEFAULT_STATE;

    default: return state;
  }
};

const actions = {
  ...standartActions,
  ...thunks,
};

// Export.
export {
  actions,
  selectors,
  standartActions,
};
export default reducer;
