import { RootState } from '@/entities/root';

export const selectEmail = (state: RootState) => (
  state.logIn.email
);

export const selectPassword = (state: RootState) => (
  state.logIn.password
);

export const selectError = (state: RootState) => (
  state.logIn.error
);
