import { actions as authActions } from '@/entities/auth';
import { actions as logInActions } from '@/entities/logIn';
import { RootAction, RootState } from '@/entities/root';
import * as selectors from './selectors';
import { ThunkAction } from 'redux-thunk';
import { logIn as logInService, LogInData, LogInResponse, LogInError } from '@/services/logIn';

export const logIn = (data: LogInData): ThunkAction<Promise<void> | void, RootState, undefined, RootAction> => {
  return (dispatch, getState) => {
    if (selectors.selectError(getState())) {
      dispatch(logInActions.changeError(''));
    }

    return logInService(data).then(
      (response: LogInResponse) => {
        dispatch(logInActions.clearForm());
        dispatch(authActions.logIn(response) as any);
      },
      (error: LogInError) => {
        dispatch(logInActions.changeError(error.message));
      },
    );
  };
};
