import { combineReducers } from 'redux';
import { ActionType, StateType } from 'typesafe-actions';
import auth from '../auth';
import employees, { standartActions as employeesActions } from '../employees';
import logIn, { standartActions as logInActions } from '../logIn';

type RootState = StateType<typeof reducer>;

type RootAction = ActionType<
  typeof employeesActions |
  typeof logInActions
>;

const reducer = combineReducers({
  auth,
  employees,
  logIn,
});

// Export.
export {
  RootAction,
  RootState,
};
export default reducer;
