declare module '*.css';
declare module '*.sass';
declare module '*.jpeg';
declare module '*.json';
declare module '*.jpg';
declare module '*.png';
declare module '*.svg';
declare module '*.ttf';
declare module '*.eot';
declare module '*.woff';

type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;
