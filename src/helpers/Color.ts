/**
 * A color of the application.
 */
enum Color {

  /**
   * White color.
   */
  white = '#fff',

  primaryText = '#222',
}

// Export.
export default Color;
