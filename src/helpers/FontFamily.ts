/**
 * Font families of the appilcation.
 */
enum FontFamily {
  /**
   * Primary font of the application.
   */
  primary = "'Open Sans', sans-serif",

  /**
   * Secondary font of the application.
   */
  secondary = '',
}

// Export.
export default FontFamily;
