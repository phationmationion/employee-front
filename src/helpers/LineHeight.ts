/**
 * Line height of the text.
 */
enum LineHeight {
  /**
   * Regular line height.
   */
  primary = 1.43,

  /**
   * Line height with an elevated value.
   */
  secondary = 1.71,
}

// Export.
export default LineHeight;
