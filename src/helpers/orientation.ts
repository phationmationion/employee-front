const orientationHelper = {
  lock(orientation: OrientationLockType): void {
    // @ts-ignore
    if (ScreenOrientation && typeof ScreenOrientation.lock === 'function') {
      // @ts-ignore
      ScreenOrientation.lock(orientation);
    } else if (screen) {
      // @ts-ignore
      if (typeof screen.lockOrientation === 'function') {
        // @ts-ignore
        screen.lockOrientation(orientation);
      // @ts-ignore
      } else if (typeof screen.msLockOrientation === 'function') {
        // @ts-ignore
        screen.msLockOrientation(orientation);
      } else if (screen.orientation && typeof screen.orientation.lock === 'function') {
        screen.orientation.lock(orientation).catch(() => {});
      }
    }
  },
  unlock() {
    // @ts-ignore
    if (ScreenOrientation && typeof ScreenOrientation.unlock === 'function') {
      // @ts-ignore
      ScreenOrientation.unlock();
    } else if (screen) {
      // @ts-ignore
      if (typeof screen.unlockOrientation === 'function') {
        // @ts-ignore
        screen.unlockOrientation();
        // @ts-ignore
      } else if (typeof screen.msUnlockOrientation === 'function') {
        // @ts-ignore
        screen.msUnlockOrientation();
      } else if (screen.orientation && typeof screen.orientation.unlock === 'function') {
        screen.orientation.unlock();
      }
    }
  },
  getType() {
    // @ts-ignore
    if (ScreenOrientation && ScreenOrientation.type) {
      // @ts-ignore
      return ScreenOrientation.type;
    }
    if (screen && screen.orientation) {
      return screen.orientation.type;
    }
  },
};

export default orientationHelper;
