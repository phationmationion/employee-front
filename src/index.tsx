import '@/styles.sass';
import 'antd/dist/antd.css';
import createStore from '@/createStore';
import Routes from '@/Routes';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { StateType } from 'typesafe-actions';

type Store = StateType<typeof store>;

const store = createStore();

render(
  <Provider store={store}>
    <Router>
      <Routes />
    </Router>
  </Provider>,
  document.getElementById('root'),
);

// Export.
export { Store };
