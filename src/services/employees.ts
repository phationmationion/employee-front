import { Moment } from 'moment';

export interface EmployeesFetchData {
  date?: [Moment, Moment];
  object?: string;
  objectMatch?: string;
  status?: string;
  statusMatch?: string;
  pageSize: number;
  current?: number;
  total?: number;
  sortField?: string;
  sortOrder?: string;
}

export interface EmployeeAction {
  id: string;
  name: string;
}

export interface Employee {
  firstName: string;
  lastName: string;
  actions: EmployeeAction[];
  id: string;
}

const employeesMockData = {
  columns: [{
    title: 'ФИО кандидата',
    dataIndex: 'name',
    fixed: 'left',
    render: (_: any, employee: Employee) => `${employee.firstName} ${employee.lastName}`,
    width: 150,
  }, {
    title: 'Дата направления',
    dataIndex: 'date',
    width: 180,
  }, {
    title: 'Объект',
    dataIndex: 'object',
    width: 170,
  }, {
    title: 'Статус',
    dataIndex: 'status',
    width: 160,
  }, {
    title: 'Контакты',
    dataIndex: 'contacts',
    width: 180,
  }],

  list: [{
    date: '1999',
    firstName: 'Keith',
    lastName: 'Richards',
    object: 'Guitar',
    status: '1950',
    contacts: 'Контакты есть',
    actions: [
      { id: '1', name: 'Действие 1' },
      { id: '2', name: 'Действие 2' },
      { id: '3', name: 'Действие 3' },
    ],
    key: 0,
  }],
};

export const getEmployees = (form?: EmployeesFetchData): Promise<typeof employeesMockData> => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(employeesMockData), 500);
  });
};
