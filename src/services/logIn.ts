export interface LogInData {
  email: string;
  password: string;
}

export interface LogInResponse {
  access: {
    token: string;
    exp: string;
  };
  refresh: {
    token: string;
    exp: string;
  };
}

export interface LogInError {
  message: string;
}

const responseMock = {
  access: {
    token: 'string',
    exp: 'string',
  },
  refresh: {
    token: 'string',
    exp: 'string',
  },
};

const errorMock = {
  message: 'Введены неверные логин/пароль',
};

const validMockData = {
  email: '1234',
  password: '1234',
};

export const logIn = (data: LogInData): Promise<LogInResponse> => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (data.email === validMockData.email && data.password === validMockData.password) {
        resolve(responseMock);
      } else {
        reject(errorMock);
      }
    }, 500);
  });
};
