export interface Object {
  id: string;
  name: string;
}

export interface ObjectsResponse {
  total: number;
  list: Object[];
}

const objectsData = {
  total: 1,
  list: [
    {
      id: '1950',
      name: 'Тест',
    },
  ],
};

export const getObjects = (): Promise<ObjectsResponse> => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(objectsData), 500);
  });
};
