export type Status = {
  id: string;
  name: string;
};

export interface StatusesResponse {
  total: number;
  list: Status[];
}

export interface PostStatusData {
  id_employee: string;
  id_status: string;
}

const statusesData = {
  total: 1,
  list: [
    {
      id: '1950',
      name: 'Тест',
    },
  ],
};

export const getStatuses = (): Promise<StatusesResponse> => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(statusesData), 500);
  });
};

export const postStatus = (data: PostStatusData): Promise<undefined> => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), 500);
  });
};
