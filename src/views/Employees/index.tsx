import React from 'react';

import { lockMessageText } from '@/content/employees.json';

import DateContainer from '@/containers/employees/Date';
import ObjectContainer from '@/containers/employees/Object';
import PageSize from '@/containers/employees/PageSize';
import Status from '@/containers/employees/Status';
import Table from '@/containers/employees/Table';
import PageWrapper from '@/containers/page/PageWrapper';

import Filters from '@/components/employees/Filters';
import LockMessage from '@/components/employees/LockMessage';
import Spacer from '@/components/common/Spacer';
import OrientationLock from '@/components/common/OrientationLock';

const Employees = () => (
  <PageWrapper>
    <OrientationLock
      orientation="landscape"
      lockMessage={
        <LockMessage>
          {lockMessageText}
        </LockMessage>
      }
    >
      <Filters
        object={<ObjectContainer />}
        date={<DateContainer />}
        status={<Status />}
        pageSize={<PageSize />}
      />
      <Spacer height={20} />
      <Table />
    </OrientationLock>
  </PageWrapper>
);

export default Employees;
