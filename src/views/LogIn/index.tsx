import React from 'react';

import Form from '@/containers/logIn/Form';
import PageWrapper from '@/containers/page/PageWrapper';
import Spacer from '@/components/common/Spacer';

const LogIn = () => (
  <PageWrapper centeredLogo={true}>
    <Form />

    <Spacer height={30} />
  </PageWrapper>
);

export default LogIn;
